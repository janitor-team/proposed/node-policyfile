var http = require('http')
  , fspfs = require('..');

// random port
var port = 24000 + Math.floor(Math.random() * (2000 + 1));

var server = http.createServer(function(q,r){ r.writeHead(200); r.end(':3') }) 
  , flash = fspfs.createServer();



server.listen(port);
flash.listen(port+1,server);
server.close()
flash.close()
